var app = angular.module('mealtrack.services.meals',[]);


app.service("MealService", function () {
    
    var meals = [{
    id: 0,
    title: 'Meal 1',
    category: 'Lunch',
    picture: null
  }, {
    id: 1,
    title: 'Meal 2',
    category: 'Lunch',
    picture: null
  }, {
    id: 2,
    title: 'Meal 3',
    category: 'Lunch',
    picture: null
  }, {
    id: 3,
    title: 'Meal 4',
    category: 'Lunch',
    picture: null
  }, {
    id: 4,
    title: 'Meal 5',
    category: 'Lunch',
    picture: null
  }];
    
    
    
    var self = {
        
        'load' : function(){
            
          return meals;  
            
        }
        
    }
    
    
    return self;
})